/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.ManejadorEntradas;
import Modelo.ManejadorUsuarios;
import Modelo.Sistema;
import Modelo.Usuario;
import Vista.ListaAmigos;
import Vista.ListaNoAmigos;
import Vista.ListaSolicitudes;
import Vista.Login;
import Vista.Muro;
import Vista.MuroAmigo;
import Vista.Perfil;
import Vista.PerfilSolicitado;
import Vista.PerfilSolicitante;
import Vista.RegistroNuevosUsuarios;
import Vista.SeleccionarPlantilla;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import javax.swing.UIManager;
import net.sourceforge.napkinlaf.NapkinLookAndFeel;

/**
 *
 * @author Elías
 */
public class Controlador {

    //Ventanas
    private Perfil perfil;
    private PerfilSolicitante perfilSolicitante;
    private PerfilSolicitado perfilSolicitado;
    private Login login;
    private Muro muro;
    private List<MuroAmigo> murosAmigo;
    private ListaAmigos listaAmigos;
    private ListaNoAmigos listaNoAmigos;
    private ListaSolicitudes listaSolicitudes;
    private RegistroNuevosUsuarios registro;
    private SeleccionarPlantilla plantilla;
    //Manejadores
    private ManejadorUsuarios manejadorU;
    private ManejadorEntradas manejadorE;
    //Modelo
    private Sistema m;

    private Usuario usuario_logeado;

    public void cambiar_a_muro() {
        muro.setNombre(usuario_logeado.getNombre());
        muro.setEntradas(usuario_logeado);
        muro.setVisible(true);
    }

    public void register() {
        login.dispose();
        registro.setVisible(true);
    }

    private void next(String nombre, String apellidos, String fecha, String localidad, String pais, String email, String contrasena) {
        if (manejadorU.darseDeAlta(nombre, apellidos, fecha, localidad, pais, email, contrasena) == 0) {
            registro.dispose();
            plantilla.setVisible(true);
        }
    }

    public void login(String email) {
        if (manejadorU.login(email) == 0) {
            login.dispose();
            usuario_logeado = m.getUsuario(email);
            cambiar_a_muro();
        }
    }

    public void logout() {
        //Cerramos todas las ventanas
        muro.dispose();
        perfil.dispose();
        perfilSolicitante.dispose();
        perfilSolicitado.dispose();
        listaAmigos.dispose();
        listaNoAmigos.dispose();
        listaSolicitudes.dispose();
        registro.dispose();
        plantilla.dispose();
        //Mostramos la de login
        login.setVisible(true);
    }

    public void confirm() {
        plantilla.dispose();
        cambiar_a_muro();
    }

    public void amigos(String email) {
        listaAmigos.setEmail(email);
        listaAmigos.setListaAmigos(m.getUsuario(email));
        listaAmigos.setVisible(true);
    }

    public void buscar() {
        listaNoAmigos.setEmail(usuario_logeado.getEmail());
        listaNoAmigos.setListaNoAmigos(m, usuario_logeado);
        listaNoAmigos.setVisible(true);
    }

    public void perfilSolicitante(String email) {
        Usuario u = m.getUsuario(email);
        perfilSolicitante.setNombre(u.getNombre());
        perfilSolicitante.setApellidos(u.getApellidos());
        perfilSolicitante.setFecha(u.getFechaDeNacimiento());
        perfilSolicitante.setEmail(u.getEmail());
        perfilSolicitante.setLocalidad(u.getLocalidad());
        perfilSolicitante.setPais(u.getPais());
        perfilSolicitante.setVisible(true);
    }

    public void perfilSolicitado(String email) {
        Usuario u = m.getUsuario(email);
        perfilSolicitado.setNombre(u.getNombre());
        perfilSolicitado.setApellidos(u.getApellidos());
        perfilSolicitado.setFecha(u.getFechaDeNacimiento());
        perfilSolicitado.setEmail(u.getEmail());
        perfilSolicitado.setLocalidad(u.getLocalidad());
        perfilSolicitado.setPais(u.getPais());
        perfilSolicitado.setVisible(true);
    }

    public void perfil(String email) {
        perfil.setVisibilidadBoton(!email.equals(usuario_logeado.getEmail()));
        perfil.setEstadoEditable(email.equals(usuario_logeado.getEmail()));
        perfil.setGuardarVisible(email.equals(usuario_logeado.getEmail()));
        Usuario u = m.getUsuario(email);
        perfil.setNombre(u.getNombre());
        perfil.setApellidos(u.getApellidos());
        perfil.setFecha(u.getFechaDeNacimiento());
        perfil.setEmail(u.getEmail());
        perfil.setLocalidad(u.getLocalidad());
        perfil.setPais(u.getPais());
        perfil.setVisible(true);
    }

    public void perfilAmigo(String email) {
        perfil.setVisibilidadBoton(false);
        Usuario u = m.getUsuario(email);
        perfil.setNombre(u.getNombre());
        perfil.setApellidos(u.getApellidos());
        perfil.setFecha(u.getFechaDeNacimiento());
        perfil.setEmail(u.getEmail());
        perfil.setLocalidad(u.getLocalidad());
        perfil.setPais(u.getPais());
        perfil.setVisible(true);
    }

    public void solicitudes() {
        listaSolicitudes.setEmail(usuario_logeado.getEmail());
        listaSolicitudes.setSolicitudes(usuario_logeado);
        listaSolicitudes.setVisible(true);
    }

    public void publicar(String email, String emails, String texto) {
        manejadorE.publicarEntrada(email, emails, texto, "Texto");
        muro.setEntradas(m.getUsuario(emails));
        muro.setTexto("");
    }

    public void publicarAmigo(String email, String emails, String texto, MuroAmigo muroAmigo) {
        manejadorE.publicarEntrada(email, emails, texto, "Texto");
        muroAmigo.setEntradas(m.getUsuario(emails));
        muroAmigo.setTexto("");
    }

    public void aceptar(String email) {
        //email representa el email del solicitante
        manejadorU.responderSolicitud(usuario_logeado.getEmail(), email, true);
        listaSolicitudes.removeSelectedSolicitud();
        if (perfilSolicitante.isVisible()) {
            perfilSolicitante.dispose();
        }
        //perfilSolicitante.dispose();
    }

    public void rechazar(String email) {
        //email representa el email del solicitante
        manejadorU.responderSolicitud(usuario_logeado.getEmail(), email, false);
        listaSolicitudes.removeSelectedSolicitud();
        //perfilSolicitante.dispose();
        if (perfilSolicitante.isVisible()) {
            perfilSolicitante.dispose();
        }
    }

    public void solicitar(String email) {
        //email representa el email del solicitado
        manejadorU.solicitarAmistad(usuario_logeado.getEmail(), email);
        listaNoAmigos.removeSelectedIndex();
        //perfilSolicitado.dispose();
    }

    public void muroAmigo(String email) {
        for (MuroAmigo ma : murosAmigo) {
            if (ma.getEmail().equals(email)) {
                if (!ma.isVisible()) {
                    perfil.dispose();
                    ma.setVisible(true);
                }
                return;
            }
        }
        MuroAmigo muroAmigo = new MuroAmigo();
        muroAmigo.setNombre(m.getUsuario(email).getNombre());
        muroAmigo.setEmail(email);
        muroAmigo.setEntradas(m.getUsuario(email));
        muroAmigo.setVisible(true);
        muroAmigo.setAmigosButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                amigos(muroAmigo.getEmail());
            }
        });
        muroAmigo.setPerfilButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                perfilAmigo(muroAmigo.getEmail());
            }
        });
        muroAmigo.setPublicarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publicarAmigo(usuario_logeado.getEmail(), muroAmigo.getEmail(), muroAmigo.getTexto(), muroAmigo);
            }
        });
        murosAmigo.add(muroAmigo);
        perfil.dispose();
    }

    public void guardarEstado(){
        usuario_logeado.getPerfil().setFraseEstado(perfil.getEstado());
                
    }
    
    
    public Controlador() {

        m = new Sistema();
        //Inicio Ventanas
        login = new Login();
        muro = new Muro();
        murosAmigo = new ArrayList<>();
        registro = new RegistroNuevosUsuarios();
        plantilla = new SeleccionarPlantilla();
        listaAmigos = new ListaAmigos();
        listaNoAmigos = new ListaNoAmigos();
        listaSolicitudes = new ListaSolicitudes();
        perfil = new Perfil();
        perfilSolicitante = new PerfilSolicitante();
        perfilSolicitado = new PerfilSolicitado();
        //Fin Ventanas
        //Manejadores
        manejadorU = new ManejadorUsuarios(m);
        manejadorE = new ManejadorEntradas(m);
        muro.setLogoutButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });
        muro.setAmigosButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                amigos(usuario_logeado.getEmail());
            }
        });

        muro.setBuscarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buscar();
            }
        });

        muro.setPerfilButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                perfil(usuario_logeado.getEmail());
            }
        });

        muro.setSolicitudesButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                solicitudes();
            }
        });
        muro.setPublicarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publicar(usuario_logeado.getEmail(), usuario_logeado.getEmail(), muro.getTexto());
            }
        });
        login.setLoginButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login(login.getLogin());
            }
        });
        login.setRegisterButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                register();
            }
        });
        registro.setNextButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next(registro.getNameEntry(), registro.getApellidos(), registro.getFecha(), registro.getLocalidad(),
                        registro.getPais(), registro.getEmail(), "");
            }
        });
        plantilla.setConfirmButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirm();
            }
        });
        perfilSolicitante.setAceptarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar(perfilSolicitante.getEmail());
            }
        });
        perfilSolicitante.setRechazarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rechazar(perfilSolicitante.getEmail());
            }
        });

        perfilSolicitado.setSolicitarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                solicitar(perfilSolicitado.getEmail());
            }
        });
        listaSolicitudes.setRechazarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listaSolicitudes.getSelectedIndex() != -1) {
                    rechazar(usuario_logeado.getSolicitudes().get(listaSolicitudes.getSelectedIndex()).getEmail());
                }
            }
        });
        listaSolicitudes.setAceptarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listaSolicitudes.getSelectedIndex() != -1) {
                    aceptar(usuario_logeado.getSolicitudes().get(listaSolicitudes.getSelectedIndex()).getEmail());
                }
            }
        });
        listaSolicitudes.setVerPerfilListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listaSolicitudes.getSelectedIndex() != -1) {
                    perfilSolicitante(usuario_logeado.getSolicitudes().get(listaSolicitudes.getSelectedIndex()).getEmail());
                }
            }
        });
        listaAmigos.setVerPerfilButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listaAmigos.getSelectedIndex() != -1) {
                    perfil(m.getUsuario(listaAmigos.getEmail()).getAmigos().get(listaAmigos.getSelectedIndex()).getEmail());
                }
            }
        });
        listaNoAmigos.setSolicitarListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Usuario> noAmigos = new ArrayList<>();
                noAmigos.addAll(m.getUsuarios());
                noAmigos.removeAll(usuario_logeado.getAmigos());
                noAmigos.removeAll(usuario_logeado.getSolicitudes());
                noAmigos.remove(usuario_logeado);
                if (listaNoAmigos.getSelectedIndex() != -1) {
                    solicitar(noAmigos.get(listaNoAmigos.getSelectedIndex()).getEmail());
                }
            }
        });
        listaNoAmigos.setPerfilListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Usuario> noAmigos = new ArrayList<>();
                noAmigos.addAll(m.getUsuarios());
                noAmigos.removeAll(usuario_logeado.getAmigos());
                noAmigos.removeAll(usuario_logeado.getSolicitudes());
                noAmigos.remove(usuario_logeado);
                if (listaNoAmigos.getSelectedIndex() != -1) {
                    Usuario solicitado = noAmigos.get(listaNoAmigos.getSelectedIndex());
                    perfilSolicitado(solicitado.getEmail());
                }
            }
        });
        perfil.setVerMuroListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                muroAmigo(perfil.getEmail());
            }
        });
        perfil.setGuardarListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarEstado();
            }
        });
    }

    public static void main(String[] args) {        
        Controlador c = new Controlador();
        c.login.setVisible(true);
        c.login.setLogin("eduardo@udc.es");
    }
}
