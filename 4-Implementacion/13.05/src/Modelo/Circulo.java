/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.util.List;

/**
 *
 * @author Elías
 */
public class Circulo {
    private String nombre;
    private String descripcion;
    private List<Usuario> usuarios;
    private String login_dueño;

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public String getLogin_dueño() {
        return login_dueño;
    }

    public void setLogin_dueño(String login_dueño) {
        this.login_dueño = login_dueño;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
