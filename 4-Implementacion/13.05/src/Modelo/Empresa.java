/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author Elías
 */
public class Empresa extends Usuario {

    private List<Anuncio> anuncios;

    public Empresa(String nombre, String apellidos, String fecha, String localidad, String pais, String email, String contrasena) {
        super(nombre, apellidos, fecha, localidad, pais, email, contrasena);
    }

    public List<Anuncio> getAnuncios() {
        return anuncios;
    }

    public void setAnuncios(List<Anuncio> anuncios) {
        this.anuncios = anuncios;
    }
}
