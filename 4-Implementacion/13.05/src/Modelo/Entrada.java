/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Elías
 */
public class Entrada {
    private String fechaPublicacion;
    private String tipoEntrada;
    private String texto;
    private String nombre_creador;
    private String visibilidad;
    private List<Comentario> comentarios;
    private Categoria categoria;

    public String getNombre() {
        return nombre_creador;
    }

    public void setNombre(String nombre) {
        this.nombre_creador = nombre;
    }

    
    
    public Entrada(String nombre,String texto,String tipo){
        this.texto = texto;
        this.tipoEntrada = tipo;
        this.nombre_creador = nombre;
    }
    
    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getVisibilidad() {
        return visibilidad;
    }

    public void setVisibilidad(String visibilidad) {
        this.visibilidad = visibilidad;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }
    
}
