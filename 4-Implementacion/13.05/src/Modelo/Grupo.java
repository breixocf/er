/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Elías
 */
public class Grupo {
    private List<Usuario> usuarios;
    private List<Grupo> subgrupos;
    private String nombre;
    private String fechaCreacion;
    private Muro muro;
    private Administrador_grupo administrador;

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Administrador_grupo getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador_grupo administrador) {
        this.administrador = administrador;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Grupo> getSubgrupos() {
        return subgrupos;
    }

    public void setSubgrupos(List<Grupo> subgrupos) {
        this.subgrupos = subgrupos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Muro getMuro() {
        return muro;
    }

    public void setMuro(Muro muro) {
        this.muro = muro;
    }
    
}
