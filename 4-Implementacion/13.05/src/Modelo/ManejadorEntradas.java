/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Elías
 */
public class ManejadorEntradas {

    private Sistema sistema;

    public ManejadorEntradas(Sistema s) {
        this.sistema = s;
    }

    public int publicarEntrada(String email, String emails, String texto, String tipo) {
        //email representa el email del publicador
        //emails representa el email del publicado
        Usuario publicador, publicado;
        if (((publicador = sistema.getUsuario(email)) != null) && ((publicado = sistema.getUsuario(emails)) != null)) {
            publicado.addEntrada(new Entrada(publicador.getNombre(), texto, tipo));
            return 0;
        }
        return -1;
    }
}
