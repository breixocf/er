/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author Elías
 */
public class ManejadorUsuarios {

    private Sistema sistema;
    
    public ManejadorUsuarios(Sistema s){
        this.sistema = s;
    }
    

    public int darseDeAlta(String nombre, String apellidos, String fecha, String localidad, String pais, String email, String contrasena) {
        if (sistema.getUsuario(email) == null) {
            //El usuario no existe
            sistema.addUsuario(new Usuario(nombre, apellidos, fecha, localidad, pais, email, contrasena));
            return 0;
        }
        //El usuario ya existe
        System.out.println("ERROR. El usuario ya se encuentra registrado.");
        return -1;
    }

    public int darseDeAltaEmpresa(String nombre, String localidad, String pais, String email, String contrasena) {
        if (sistema.getUsuario(email) == null) {
            sistema.addUsuario(new Empresa(nombre, null, null, localidad, pais, email, contrasena));
            return 0;
        }
        System.out.println("ERROR. La empresa ya se encuentra registrada.");
        return -1;
    }

    public int darseDeAltaDNI(String email, String contrasena) {
        return 0;
    }

    public int verAmigosUsuario(String email) {
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            u.getAmigos();
            return 0;
        }
        return -1;
    }

    public int verPerfilUsuario(String email) {
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            u.getPerfil();
            return 0;
        }
        return -1;
    }

    public int darseDeBaja(String email) {
        if (sistema.getUsuario(email) != null) {
            //El usuario no existe
            sistema.removeUsuario(email);
            return 0;
        }
        //El usuario ya existe
        System.out.println("ERROR. El usuario no está registrado.");
        return -1;
    }

    public List<Usuario> consultarSolicitudes(String email) {
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            return u.getSolicitudes();
        }
        return null;
    }

    public int responderSolicitud(String email, String emails, boolean respuesta) {
        //email representa el solicitado
        //emails representa el solicitante
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            u.responderSolicitud(emails, respuesta);
            return 0;
        }
        return -1;
    }

    public int solicitarAmistad(String email, String emails) {
        //email representa el solicitante
        //emails representa el solicitado
        Usuario solicitante, solicitado;
        if (((solicitante = sistema.getUsuario(email)) != null) && ((solicitado = sistema.getUsuario(emails)) != null)) {
            solicitante.solicitarAmistad(solicitado);
            return 0;
        }
        return -1;
    }

    public List<Usuario> obtenerUsuarios(String email) {
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            return sistema.obtenerUsuarios(u);
        }
        return null;
    }

    public int login(String email) {
        Usuario u;
        if ((u = sistema.getUsuario(email)) != null) {
            return 0;
        }
        return -1;
    }

}
