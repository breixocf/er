/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Elías
 */
public class Muro {

    private List<Entrada> entradas;

    public Muro(){
        entradas = new ArrayList<>();
    }
    
    public List<Entrada> getEntradas() {
        return entradas;
    }

    public void setEntradas(List<Entrada> entradas) {
        this.entradas = entradas;
    }
    
    public void addEntrada(Entrada entrada){
        entradas.add(0, entrada);
        //entradas.add(entrada);
    }
}
