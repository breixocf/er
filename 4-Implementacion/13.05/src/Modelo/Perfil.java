/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Elías
 */
public class Perfil {

    private String fraseEstado;
    private Plantilla plantilla;

    public Perfil() {
        fraseEstado = "";
    }

    public String getFraseEstado() {
        return fraseEstado;
    }

    public void setFraseEstado(String fraseEstado) {
        this.fraseEstado = fraseEstado;
    }

    public Plantilla getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(Plantilla plantilla) {
        this.plantilla = plantilla;
    }

}
