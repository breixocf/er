/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Elías
 */
public class Sistema {

    private List<Usuario> usuarios;
    private Chat chat;

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public Sistema() {
        usuarios = new ArrayList<>();
        Usuario u = new Usuario("Eduardo", "eduardo@udc.es");
        u.getMuro().addEntrada(new Entrada("Eduardo", "Prueba1", "Texto"));
        u.getMuro().addEntrada(new Entrada("Eduardo", "Prueba2", "Texto"));
        u.getMuro().addEntrada(new Entrada("Eduardo", "Prueba3", "Texto"));
        u.getMuro().addEntrada(new Entrada("Eduardo", "Prueba4", "Texto"));
        usuarios.add(u);
        u = new Usuario("PruebaSolicitante1","prueba1@udc.es");
        usuarios.add(u);
        u.solicitarAmistad(usuarios.get(0));
        u = new Usuario("PruebaAmigo1","prueba2@udc.es");
        usuarios.add(u);
        u.solicitarAmistad(usuarios.get(0));
        usuarios.get(0).responderSolicitud(u.getEmail(), true);
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void addUsuario(Usuario usuario) {
        this.usuarios.add(usuario);
    }

    public void removeUsuario(String email) {
        for (Usuario u : usuarios) {
            if (u.getEmail().equals(email)) {
                usuarios.remove(u);
                break;
            }
        }
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Usuario getUsuario(String email) {
        for (Usuario u : usuarios) {
            if (u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

    public List<Usuario> obtenerUsuarios(Usuario usuario) {
        List<Usuario> resultado = new ArrayList<>();
        for (Usuario u : usuarios) {
            if (usuario.getAmigos().indexOf(u) == -1) {
                resultado.add(u);
            }
        }
        return resultado;
    }

}
