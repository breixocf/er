/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Elías
 */
public class Usuario {

    private String contrasena;
    private String dni;
    private String nombre;
    private String apellidos;
    private String fechaDeNacimiento;
    private String pais;
    private String localidad;
    private String email;
    private Muro muro;
    private Moneda moneda;
    private Perfil perfil;
    private List<Album> albumes;
    private List<Usuario> amigos;
    private List<Circulo> circulos;
    private List<Fichero> ficheros;
    private List<Usuario> solicitudes;  
     
    public Usuario(String nombre,String email) {
        this.nombre = nombre;
        this.email = email;
        this.muro = new Muro();
        this.perfil = new Perfil();
        albumes = new ArrayList<>();
        amigos = new ArrayList<>();
        circulos = new ArrayList<>();
        ficheros = new ArrayList<>();
        solicitudes = new ArrayList<>();
    }

    public Usuario(String nombre, String apellidos, String fecha, String localidad, String pais, String email, String contrasena) {
        this(nombre,email);        
        this.apellidos = apellidos;
        this.fechaDeNacimiento = fecha;
        this.localidad = localidad;
        this.pais = pais;        
        this.contrasena = contrasena;                        
    }

    public List<Usuario> getSolicitudes() {
        return solicitudes;
    }

    public Usuario getSolicitud(String email) {
        for (Usuario u : solicitudes) {
            if (u.email.equals(email)) {
                return u;
            }
        }
        return null;
    }

    public void addSolicitud(Usuario solicitador) {
        this.solicitudes.add(solicitador);
    }

    public void setSolicitudes(List<Usuario> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public List<Circulo> getCirculos() {
        return circulos;
    }

    public void setCirculos(List<Circulo> circulos) {
        this.circulos = circulos;
    }

    public List<Fichero> getFicheros() {
        return ficheros;
    }

    public void setFicheros(List<Fichero> ficheros) {
        this.ficheros = ficheros;
    }

    public List<Album> getAlbumes() {
        return albumes;
    }

    public void addAlbum(Album album) {
        this.albumes.add(album);
    }

    public Muro getMuro() {
        return muro;
    }

    public void setMuro(Muro muro) {
        this.muro = muro;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public List<Usuario> getAmigos() {
        return amigos;
    }

    public void setAmigos(List<Usuario> amigos) {
        this.amigos = amigos;
    }

    public String getContraseña() {
        return contrasena;
    }

    public void setContraseña(String contraseña) {
        this.contrasena = contraseña;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(String fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void verificarDocumento(String dni) {

    }

    public void solicitarAmistad(Usuario solicitado) {
        solicitado.addSolicitud(this);
    }

    public void addEntrada(Entrada entrada) {
        muro.addEntrada(entrada);
    }

    public void responderSolicitud(String emails, boolean respuesta) {
        Usuario usuario = null;
        //Comprobamos que el email recibido en el método se encuentre
        //dentro de la lista de solicitudes
        for (Usuario u : solicitudes) {
            if (u.getEmail().equals(emails)) {
                usuario = u;
                break;
            }
        }
        if (usuario != null) {
            if (respuesta) {
                amigos.add(usuario);
                usuario.amigos.add(this);
            }
            solicitudes.remove(usuario);
        }
    }

}
